//
//  Service.swift
//  Food2fork
//
//  Created by NGUYEN Huu Viet on 18/07/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import Foundation

enum JSONError: String, Error {
    case NoData = "ERROR: no data"
    case ConversionFailed = "ERROR: conversion from JSON failed"
}

class Service: NSObject {
    static let shared = Service()
    
    func fetchRecipes(completion: @escaping ([Recipe]?, Error?) -> ()) {
        let urlString = "https://www.food2fork.com/api/search?key=4a5e0c1d486fa9ddbb271a5229e18455&q=shredded%20chicken"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch courses:", err)
                return
            }
            
            // check response
            
            guard let data = data else { return }
            do {
                
                print(data)
                let serverResponse = try JSONDecoder().decode(ServerResponse.self, from: data)
                
                let recipes = serverResponse.recipes
                
                print(recipes)
                DispatchQueue.main.async {
                    completion(recipes, nil)
                }
            } catch let jsonErr {
                print("Failed to decode1:", jsonErr)
            }
            }.resume()
    }
    
    func fetchRecipeById(recipeId: String, completion: @escaping(RecipeDetail?, Error?) -> ()) {
        let urlString = "https://www.food2fork.com/api/get?key=4a5e0c1d486fa9ddbb271a5229e18455&rId=\(recipeId)"
        
        print(urlString)
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch courses:", err)
                return
            }
            
            // check response
            
            guard let data = data else { return }
            do {
                let serverResponse = try JSONDecoder().decode(RecipeGeneral.self, from: data)
                let recipeDetail = serverResponse.recipe
                DispatchQueue.main.async {
                    completion(recipeDetail, nil)
                }
            } catch let jsonErr {
                print("Failed to decode2:", jsonErr)
            }
            }.resume()
    }
}
