//
//  ViewController.swift
//  Food2fork
//
//  Created by NGUYEN Huu Viet on 17/07/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let cellId = "cellId"
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var recipeViewModelController: RecipeViewModelController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
  //      setupNavBar()
        setupSearchBar()
        setupTableView()
        recipeViewModelController = RecipeViewModelController.init(reloadData: self)
    }
    
    fileprivate func setupSearchBar() {
        searchBar.delegate = self
        searchBar.returnKeyType = .done
    }

    
    fileprivate func setupNavBar() {
        navigationItem.title = "Recipes"
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.backgroundColor = .yellow
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.rgb(r: 50, g: 199, b: 242)
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    fileprivate func setupTableView() {
        let nib = UINib(nibName: "RecipeCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellId)
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
    }

}

extension ViewController : ReloadDataProtocol {
    func reloadData() {
        self.tableView.reloadData()
    }
}


extension ViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recipeViewModelController!.recipeViewModelsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! RecipeCell
        
        let recipeViewModel = self.recipeViewModelController!.recipeViewModel(at: indexPath.row)
        cell.setDataToViewCell(recipeViewModel: recipeViewModel!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        let detailReciprViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailRecipeViewController") as! DetailRecipeViewController
        detailReciprViewController.recipeId = (recipeViewModelController?.recipeViewModels[indexPath.row].recipeId)!
        self.navigationController?.pushViewController(detailReciprViewController, animated: true)
        
    }
}

extension ViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchBar.perform(#selector(UIResponder.resignFirstResponder), with: nil, afterDelay: 0)
        }
        recipeViewModelController!.recipeWithAKeyWord(keyword: searchBar.text!)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.searchBar.endEditing(true)
    }
}



extension UIColor {
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}

