//
//  DetailRecipeViewController.swift
//  Food2fork
//
//  Created by Dynseo on 18/07/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import UIKit

class DetailRecipeViewController: UIViewController {
    
    var recipeId = ""
    var recipeDetailViewModelController: RecipeDetailViewModelController?
    
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var recipeNameLabel: UILabel!
    
    @IBOutlet weak var publisherLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    
    @IBOutlet weak var sourceURLLabel: UILabel!
    
    @IBOutlet weak var ingredientsLabel: UITextView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        ingredientsLabel.isEditable = false
        recipeDetailViewModelController = RecipeDetailViewModelController.init(recipeId: recipeId, reloadDetailData: self)
    }
    
    
    func setupRecipeDetail(recipeDetail: RecipeDetailViewModel) {
        self.photoImageView.loadImageUsingUrlString(urlString:recipeDetail.photoUrl)
        self.photoImageView.layer.cornerRadius = (self.photoImageView.frame.height / 2)
        self.photoImageView.layer.masksToBounds = true
        self.recipeNameLabel.text = recipeDetail.title
        self.publisherLabel.text = recipeDetail.publisher
        self.rankLabel.text = recipeDetail.rank
        self.sourceURLLabel.text = recipeDetail.sourceUrl
        self.ingredientsLabel.text = recipeDetail.ingredients
    }
}

extension DetailRecipeViewController : ReloadDetailDataProtocol {
    func reloadDetailData() {
        setupRecipeDetail(recipeDetail: recipeDetailViewModelController!.recipeDetail!)
    }
}
