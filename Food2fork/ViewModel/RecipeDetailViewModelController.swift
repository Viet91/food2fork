//
//  RecipeDetailViewModelController.swift
//  Food2fork
//
//  Created by Dynseo on 18/07/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import Foundation

protocol ReloadDetailDataProtocol {
    func reloadDetailData()
}

class RecipeDetailViewModelController {
    var recipeDetail: RecipeDetailViewModel?
    
    var reloadDetailData: ReloadDetailDataProtocol
    init(recipeId: String, reloadDetailData: ReloadDetailDataProtocol) {
        self.reloadDetailData = reloadDetailData
        fetchDataById(recipeId: recipeId)
    }
    
    func fetchDataById(recipeId : String) {
        
        Service.shared.fetchRecipeById(recipeId: recipeId) { (recipeDetail, err) in
            if let err = err {
                print("Failed to fetch courses:", err)
                return
            }
            self.recipeDetail = RecipeDetailViewModel(recipeDetail: recipeDetail!)
            self.reloadDetailData.reloadDetailData()
        }
    }
}
