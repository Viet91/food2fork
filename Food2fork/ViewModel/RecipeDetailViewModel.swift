//
//  RecipeDetailViewModel.swift
//  Food2fork
//
//  Created by Dynseo on 18/07/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import Foundation

struct RecipeDetailViewModel {
    let photoUrl: String
    let title: String
    let publisher: String
    let rank: String
    let recipeId: String
    var ingredients: String
    let sourceUrl : String
    
    init(recipeDetail: RecipeDetail) {
        self.photoUrl = recipeDetail.image_url
        self.title = recipeDetail.title
        self.publisher = recipeDetail.publisher
        self.rank = "Rank social: \(recipeDetail.social_rank)"
        self.recipeId = recipeDetail.recipe_id
        
        self.ingredients = "Ingredient Detail: \n"
        for aString in recipeDetail.ingredients {
            self.ingredients.append(aString)
            self.ingredients.append("\n")
        }
        self.sourceUrl = "Source: \(recipeDetail.source_url)"
    }
}
