//
//  RecipeViewModel.swift
//  Food2fork
//
//  Created by NGUYEN Huu Viet on 17/07/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import Foundation

struct RecipeViewModel {
    
    let photoUrl: String
    let title: String
    let publisher: String
    let rank: String
    let recipeId: String
    init(recipe: Recipe) {
        self.photoUrl = recipe.image_url
        self.title = recipe.title
        self.publisher = recipe.publisher
        self.rank = "Rank social: \(recipe.social_rank)"
        self.recipeId = recipe.recipe_id
    }
}
