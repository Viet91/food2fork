//
//  RecipeViewModelController.swift
//  Food2fork
//
//  Created by Dynseo on 18/07/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import Foundation

protocol ReloadDataProtocol {
    func reloadData()
}

class RecipeViewModelController {
    var reloadData : ReloadDataProtocol
    var datas = [RecipeViewModel]()
    var recipeViewModels = [RecipeViewModel]()
    
    init(reloadData : ReloadDataProtocol) {
        self.reloadData = reloadData
        fetchData()
    }
    
    func fetchData() {
        Service.shared.fetchRecipes { (recipes, err) in
            if let err = err {
                print("Failed to fetch courses:", err)
                return
            }
            self.datas = recipes?.map({return RecipeViewModel(recipe: $0)}) ?? []
            self.recipeViewModels = self.datas
            self.reloadData.reloadData()
        }
    }
    
    var recipeViewModelsCount : Int {
        return recipeViewModels.count
    }
    
    func recipeViewModel (at index: Int) -> RecipeViewModel? {
        guard index >= 0 && index < recipeViewModelsCount else {
            return nil
        }
        return recipeViewModels[index]
    }
    
    func recipeWithAKeyWord(keyword : String) {
        if keyword.isEmpty {
            self.recipeViewModels = self.datas
        }
        else {
            self.recipeViewModels = datas.filter({ (recipeModelView) -> Bool in
                recipeModelView.title.contains(keyword)
            })
        }
        self.reloadData.reloadData()
    }
}
