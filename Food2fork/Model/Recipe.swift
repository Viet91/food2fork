//
//  Recipe.swift
//  Food2fork
//
//  Created by NGUYEN Huu Viet on 17/07/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import Foundation

struct ServerResponse : Decodable {
    let count: Int
    let recipes: [Recipe]
}

struct RecipeGeneral : Decodable {
    let recipe : RecipeDetail
}

struct RecipeDetail :Decodable {
    let recipe_id: String
    let image_url: String
    let title: String
    let publisher: String
    let social_rank: Double
    let source_url: String
    let ingredients: [String]
}

struct Recipe : Decodable {
    let recipe_id: String
    let image_url: String
    let title: String
    let publisher: String
    let social_rank: Double
}
