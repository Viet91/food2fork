//
//  RecipeCell.swift
//  Food2fork
//
//  Created by NGUYEN Huu Viet on 17/07/2019.
//  Copyright © 2019 NGUYEN Huu Viet. All rights reserved.
//

import UIKit

class RecipeCell: UITableViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var publisherLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    
    func setDataToViewCell(recipeViewModel: RecipeViewModel) -> Void {
        self.titleLabel?.text = recipeViewModel.title
        self.publisherLabel?.text = recipeViewModel.publisher
        self.rankLabel?.text = String(recipeViewModel.rank)
        self.photoImageView.loadImageUsingUrlString(urlString:recipeViewModel.photoUrl)
    }
}

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    func loadImageUsingUrlString(urlString: String) {
        let url = URL(string: urlString)
        
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print(error as Any)
                return
            }
            DispatchQueue.main.async {
                let imageToCache = UIImage.init(data: data!)
                imageCache.setObject(imageToCache!, forKey: urlString as AnyObject)
                self.image = imageToCache
            }
        }.resume()
        
    }
}
